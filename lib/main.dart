import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:testing/data_provider.dart';
import 'package:testing/film/presentation/screen/movie_list_screen.dart';
import 'package:testing/film/presentation/view_model/view_models.dart';
import 'package:testing/user_model.dart';

void main() {
  runApp(
    const ProviderScope(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    ),
  );
}

class HomeScreen extends ConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final data = ref.watch(userDataProvider);
    return Scaffold(
      body: data.when(
        data: ((data) {
          List<UserModel> userList = data.map((e) => e).toList();
          return ListView.builder(
            itemCount: userList.length,
            itemBuilder: (_, index) {
              return ListTile(
                title: Text(userList[index].firstName),
                subtitle: ElevatedButton(
                    onPressed: () {
                      ref.read(movieListProvider.notifier).getMovieListConfig();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MovieListScreen()));
                    },
                    child: const Text('movie')),
                trailing: CircleAvatar(
                  backgroundImage: NetworkImage(userList[index].avatar),
                ),
              );
            },
          );
        }),
        error: ((error, stackTrace) => Center(
              child: Text(error.toString()),
            )),
        loading: () => const Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
