import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:testing/services.dart';
import 'package:testing/user_model.dart';

final userDataProvider = FutureProvider<List<UserModel>>((ref) async {
  return ref.watch(userProvider).getUser();
});
