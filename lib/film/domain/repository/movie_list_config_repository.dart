import 'package:testing/film/domain/model/movie_list_model.dart';

abstract class MovieListConfigRepository {
  Future<List<MovieListModel>?> getMovieList();
}
