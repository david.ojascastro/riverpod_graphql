class MovieListModel {
  const MovieListModel({
    required this.title,
    required this.director,
    required this.releaseDate,
    required this.id,
  });

  MovieListModel.initial()
      : title = 'title',
        director = 'director',
        releaseDate = 'releaseDate',
        id = 'id';

  factory MovieListModel.fromJson(Map<String, dynamic> json) {
    return MovieListModel(
      title: json['title'],
      director: json['director'],
      releaseDate: json['releaseDate'],
      id: json['id'],
    );
  }

  final String title;
  final String director;
  final String releaseDate;
  final String id;
}
