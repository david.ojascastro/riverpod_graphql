import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:testing/film/di/dependency_injections.dart';
import 'package:testing/film/domain/model/models.dart';
import 'package:testing/film/domain/repository/repositories.dart';

final movieListProvider =
    StateNotifierProvider<MovieListViewModelNotifier, List<MovieListModel>?>(
        (ref) {
  final repository = ref.read(movieListRepositoryDI);
  return MovieListViewModelNotifier(repository);
});

class MovieListViewModelNotifier extends StateNotifier<List<MovieListModel>?> {
  MovieListViewModelNotifier(this.repository) : super([]);

  final MovieListConfigRepository repository;

  Future<void> getMovieListConfig() async {
    final value = await repository.getMovieList();

    state = value;
  }
}
