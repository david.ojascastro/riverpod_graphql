import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:testing/film/domain/model/models.dart';
import 'package:testing/film/presentation/view_model/movie_list_view_model.dart';

class MovieListScreen extends HookConsumerWidget {
  const MovieListScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final result = ref.watch(movieListProvider);

    if (result == null || result.isEmpty) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    final List<MovieListModel> hakdog = result.toList();
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: hakdog.length,
              itemBuilder: (_, index) {
                return ListTile(
                  title: Text(hakdog[index].title),
                  subtitle: Text(hakdog[index].director),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
