import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:testing/film/data/remote/remote_sources.dart';
import 'package:testing/film/data/repositories/repositories.dart';

final movieListRepositoryDI = Provider((ref) {
  return MovieListRepositorImplementation(
      remoteSource: MovieListRemoteSource());
});
