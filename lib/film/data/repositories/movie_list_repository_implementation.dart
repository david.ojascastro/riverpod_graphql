import 'package:testing/film/data/remote/remote_sources.dart';
import 'package:testing/film/domain/model/models.dart';
import 'package:testing/film/domain/repository/repositories.dart';

class MovieListRepositorImplementation implements MovieListConfigRepository {
  const MovieListRepositorImplementation(
      {required MovieListRemoteSource remoteSource})
      : _remoteSource = remoteSource;

  final MovieListRemoteSource _remoteSource;

  @override
  Future<List<MovieListModel>?> getMovieList() async {
    return _remoteSource.getMovieList();
  }
}
