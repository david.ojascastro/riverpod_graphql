import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:testing/film/domain/model/movie_list_model.dart';

class MovieListRemoteSource {
  final HttpLink httpLink =
      HttpLink('https://swapi-graphql.netlify.app/.netlify/functions/index');

  Future<List<MovieListModel>?> getMovieList() async {
    final GraphQLClient client = GraphQLClient(
      link: httpLink,
      cache: GraphQLCache(),
    );

    const getMovieList = '''
    query {
      allFilms {
        films {
          title
          director
          releaseDate
          id
        }
      }
    }
  ''';

    final options = QueryOptions(document: gql(getMovieList));

    final response = await client.query(options);
    final filmsData = response.data?['allFilms']?['films'] as List<dynamic>;

    return filmsData.map((e) => MovieListModel.fromJson(e)).toList();
  }
}
